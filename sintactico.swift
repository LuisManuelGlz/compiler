import Foundation

var simbolos = ["{", "}", "(", ")", "[", "]", ".", ":", ";", ",", "=", "'[a-zA-Z_]\\w*'"]

var comentarios = "(/\\*([^*]|[\r\n]|(\\*+([^*/]|[\r\n])))*\\*+/)|(//.*)"
var especial = "\n"
var espaciosAUno = "\\s+"

var index = 0
var lexema = ""
var lexemas = [String]()
var numeroDeSimbolo = 0
var tablaDeSimbolos = [(Int, String)]()

var escaneado = [String]()
var escaneadoT: String = ""

// queda pendiente reducir todo esto
var tokens = [
    // "^[a-zA-Z_]\\w*$": "ID",
    "^\\d+$": "ENTERO",
    "^\\d+\\.\\d+$": "REAL",
    "^'[a-zA-Z0-9_]([a-zA-z0-9\\s])+'$": "CADENA",
    "^\\($": "IPARENT",
    "^\\)$": "DPARENT",
    "^\\[$": "ICORCHET",
    "^\\]$": "DCORCHET",
    "^\\{$": "ILLAVE",
    "^begin$": "ILLAVE",
    "^\\}$": "DLLAVE",
    "^end$": "DLLAVE",
    "^if$": "CONDICION",
    "^switch$": "CONDICION",
    "^else$": "CONDICION_AUX",
    "^elif$": "CONDICION_AUX",
    "^while$": "MIENTRAS",
    "^for$": "BUCLE",
    "^foreach$": "BUCLE",
    "^do$": "BUCLE",
    "^break$": "BUCLE",
    "^continue$": "BUCLE",
    "^eq$": "RELACIONAL",
    "^!=$": "RELACIONAL",
    "^<$": "RELACIONAL",
    "^>$": "RELACIONAL",
    "^<=$": "RELACIONAL",
    "^>=$": "RELACIONAL",
    "^not$": "NEGADOR",
    "^!$": "NEGADOR",
    "^and$": "LOGICO",
    "^\\&\\&$": "LOGICO",
    "^or$": "LOGICO",
    "^\\|\\|$": "LOGICO",
    "^xor$": "LOGICO",
    "^\\+$": "ARITMETICO",
    "^\\-$": "ARITMERICO",
    "^/$": "ARITMERICO",
    "^%$": "ARITMERICO",
    "^\\+\\+$": "ARITMERICO",
    "^\\+=$": "ARITMERICO",
    "^\\-\\-$": "ARITMERICO",
    "^\\-=$": "ARITMERICO",
    "^\\*\\*$": "ARITMERICO",
    "^\\*=$": "ARITMERICO",
    "^/=$": "ARITMERICO",
    "^=$": "ASIGNACION",
    "^public$": "AMBITO_ACCESO",
    "^private$": "AMBITO_ACCESO",
    "^protected$": "AMBITO_ACCESO",
    "^global$": "AMBITO_ACCESO",
    "^class$": "CLASE",
    "^interface$": "COMPORTAMIENTO",
    "^\\.$": "PUNTO",
    "^,$": "COMA",
    "^;$": "PUNTO_COMA",
    "^:$": "PUNTUACION",
    // "^\"$": "PUNTUACION",
    "^`$": "PUNTUACION",
    "^case$": "SWITCH_AUX",
    "^default$": "SWITCH_AUX",
    "^array$": "TIPO_VAR_ARR",
    "^int$": "TIPO_VAR",
    "^dict$": "TIPO_VAR_DICT",
    "^bool$": "TIPO_VAR",
    "^char$": "TIPO_VAR",
    "^enum$": "TIPO_VAR",
    "^volatile$": "TIPO_VAR",
    "^void$": "TIPO_VAR",
    "^var$": "TIPO_VAR",
    "^string$": "TIPO_VAR",
    "^decimal$": "TIPO_VAR",
    "^float$": "TIPO_VAR",
    "^double$": "TIPO_VAR",
    "^const$": "TIPO_VAR",
    "^byte$": "TIPO_VAR",
    "^sbyte$": "TIPO_VAR",
    "^uint$": "TIPO_VAR",
    "^function$": "FUNCION",
    "^def$": "FUNCION",
    "^=>$": "FUNCION_FLECHA",
    "^lambda$": "FUNCION",
    "^then$": "EXCEPCION",
    "^try$": "EXCEPCION_TRY",
    "^catch$": "EXCEPCION_CATCH",
    "^finally$": "EXCEPCION_AUX",
    "^small$": "NUMERO_LONGITUD",
    "^long$": "NUMERO_LONGITUD",
    "^ulong$": "NUMERO_LONGITUD",
    "^ushort$": "NUMERO_LONGITUD",
    "^true$": "BOOLEANO",
    "^false$": "BOOLEANO",
    "^static$": "STATIC_VAR",
    "^yield$": "ITERADOR_AUX",
    "^List$": "ESTRUCTURA",
    "^LinkedList$": "ESTRUCTURA",
    "^Stack$": "ESTRUCTURA",
    "^Queue$": "ESTRUCTURA",
    "^Stacklloc$": "ESTRUCTURA",
    "^ValueError$": "ERROR_TIPO",
    "^TypeError$": "ERROR_TIPO",
    "^NameError$": "ERROR_TIPO",
    "^ZeroDivisionError$": "ERROR_TIPO",
    "^OsError$": "ERROR_TIPO",
    "^RuntimeError$": "ERROR_TIPO",
    "^Exception$": "ERROR_TIPO",
    "^max$": "OPERACION_NUMERICA",
    "^min$": "OPERACION_NUMERICA",
    "^avg$": "OPERACION_NUMERICA",
    "^sum$": "OPERACION_NUMERICA",
    "^roof$": "OPERACION_NUMERICA",
    "^ceil$": "OPERACION_NUMERICA",
    "^floor$": "OPERACION_NUMERICA",
    "^Thread$": "HILO",
    "^Date$": "FECHA",
    "^Datetime$": "FECHA",
    "^Year$": "FECHA",
    "^Mounth$": "FECHA",
    "^Day$": "FECHA",
    "^Hour$": "FECHA",
    "^Minutes$": "FECHA",
    "^Seconds$": "FECHA",
    "^equals$": "CHEQUEO",
    "^isEmpty$": "CHEQUEO",
    "^instanceOf$": "CHEQUEO",
    "^lengthOf$": "CHEQUEO",
    "^indexOf$": "CHEQUEO",
    "^startIndexOf$": "CHEQUEO",
    "^endIndexOf$": "CHEQUEO",
    "^count$": "CHEQUEO",
    "^isStr$": "CHEQUEO",
    "^isInt$": "CHEQUEO",
    "^isSet$": "CHEQUEO",
    "^typeOf$": "CHEQUEO",
    "^print$": "DESPLEGAR",
    "^write$": "DESPLEGAR",
    "^abstract$": "TIPO_DE_CLASE",
    "^input$": "ENTRADA",
    "^prompt$": "ENTRADA",
    "^readLine$": "ENTRADA",
    "^\\$$": "VAR_EN_CADENA",
    "^args$": "ARGUMENTO",
    "^kwargs$": "ARGUMENTO",
    "^eventArgs$": "ARGUMENTO_EVENTO",
    "^override$": "COMPLETENTO_CLASE",
    "^virtual$": "COMPLETENTO_CLASE",
    "^subscribe$": "PALABRA_CLAVE",
    "^unsubscribe$": "PALABRA_CLAVE",
    "^path$": "PALABRA_CLAVE",
    "^trim$": "PALABRA_CLAVE",
    "^bind$": "PALABRA_CLAVE",
    "^get$": "PALABRA_CLAVE",
    "^set$": "PALABRA_CLAVE",
    "^destroy$": "PALABRA_CLAVE",
    "^destructor$": "PALABRA_CLAVE",
    "^init$": "PALABRA_CLAVE",
    "^constructor$": "PALABRA_CLAVE",
    "^module$": "PALABRA_CLAVE",
    "^export$": "PALABRA_CLAVE",
    "^from$": "PALABRA_CLAVE",
    "^import$": "PALABRA_CLAVE",
    "^package$": "PALABRA_CLAVE",
    "^native$": "PALABRA_CLAVE",
    "^synchronized$": "PALABRA_CLAVE",
    "^using$": "PALABRA_CLAVE",
    "^as$": "PALABRA_CLAVE",
    "^goto$": "PALABRA_CLAVE",
    "^join$": "PALABRA_CLAVE",
    "^split$": "PALABRA_CLAVE",
    "^value$": "PALABRA_CLAVE",
    "^strict$": "PALABRA_CLAVE",
    "^async$": "PALABRA_CLAVE",
    "^await$": "PALABRA_CLAVE",
    "^readOnly$": "PALABRA_CLAVE",
    "^event$": "PALABRA_CLAVE",
    "^delegate$": "PALABRA_CLAVE",
    "^super$": "PALABRA_CLAVE",
    "^in$": "PALABRA_CLAVE",
    "^fetch$": "PALABRA_CLAVE",
    "^Schema$": "PALABRA_CLAVE",
    "^Number$": "PALABRA_CLAVE",
    "^Integer$": "PALABRA_CLAVE",
    "^String$": "PALABRA_CLAVE",
    "^Array$": "PALABRA_CLAVE",
    "^Dictionary$": "PALABRA_CLAVE",
    "^pass$": "PALABRA_CLAVE",
    "^this$": "PALABRA_CLAVE",
    "^return$": "PALABRA_CLAVE",
    "^Convert$": "PALABRA_CLAVE",
    "^new$": "PALABRA_CLAVE",
    "^extends$": "PALABRA_CLAVE",
    "^throw$": "LANZAR",
    "^throws$": "LANZAR_M",
    "^null$": "PALABRA_CLAVE",
    "^none$": "PALABRA_CLAVE",
    "^undefined$": "PALABRA_CLAVE",
    "^final$": "PALABRA_CLAVE",
    "^assert$": "PALABRA_CLAVE",
    "^obj$": "PALABRA_CLAVE",
    "^die$": "PALABRA_CLAVE",
    "^delete$": "PALABRA_CLAVE",
    "^exec$": "PALABRA_CLAVE",
    "^debugger$": "PALABRA_CLAVE",
    "^exit$": "PALABRA_CLAVE",
    "^IO$": "PALABRA_CLAVE",
    "^File$": "PALABRA_CLAVE",
    "^Mail$": "PALABRA_CLAVE",
    "^Math$": "PALABRA_CLAVE",
    "^open$": "PALABRA_CLAVE",
    "^close$": "PALABRA_CLAVE",
    "^Show$": "PALABRA_CLAVE",
    "^Hidde$": "PALABRA_CLAVE",
    "^range$": "PALABRA_CLAVE",
    "^process$": "PALABRA_CLAVE",
    "^send$": "PALABRA_CLAVE",
    "^Time$": "PALABRA_CLAVE",
    "^with$": "PALABRA_CLAVE",
    "^crypto$": "PALABRA_CLAVE",
    "^scrennX$": "PALABRA_CLAVE",
    "^screenY$": "PALABRA_CLAVE",
    "^transient$": "PALABRA_CLAVE",
    "^out$": "PALABRA_CLAVE",
    "^base$": "PALABRA_CLAVE",
    "^implicit$": "PALABRA_CLAVE",
    "^lock$": "PALABRA_CLAVE",
    "^ref$": "PALABRA_CLAVE",
    "^System$": "PALABRA_CLAVE",
    "^internal$": "PALABRA_CLAVE",
    "^clone$": "PALABRA_CLAVE",
    "^copy$": "PALABRA_CLAVE",
    "^eval$": "PALABRA_CLAVE",
    "^include_once$": "PALABRA_CLAVE",
    "^Http$": "PALABRA_CLAVE",
    "^to$": "PALABRA_CLAVE",
    "^MySqlConnection$": "PALABRA_CLAVE",
    "^MySqlCommand$": "PALABRA_CLAVE",
    "^PgSqlConnection$": "PALABRA_CLAVE",
    "^PgSqlCommand$": "PALABRA_CLAVE",
    "^MsSqlConnection$": "PALABRA_CLAVE",
    "^MsSqlCommand$": "PALABRA_CLAVE",
    "^pow$": "MATE_FUNC",
    "^sqrt$": "MATE_FUNC",
    "^exp$": "MATE_FUNC",
    "^log$": "MATE_FUNC",
    "^sin$": "MATE_FUNC",
    "^cos$": "MATE_FUNC",
    "^tan$": "MATE_FUNC",
    "^asin$": "MATE_FUNC",
    "^acos$": "MATE_FUNC",
    "^atang$": "MATE_FUNC",
    "^log10$": "MATE_FUNC",
    "Main": "PRINCIPAL"
]

//////////////////////////////
//          Léxico          //
//////////////////////////////

print("Introduce el archivo:")

// leemos lo que introduzca el usuario
if let fuente = readLine() {
    if let directorio = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

        // definimos la URL
        let ruta = directorio.appendingPathComponent("ITT/7/LyA1/compiler/" + fuente)

        // leyendo archivo
        do {
            // tratamos de leer el archivo con la URL
            var texto = try String(contentsOf: ruta, encoding: .utf8)
            
            // este espacio es necesario, de lo contrario no se leera el último lexema
            texto = texto + " "

            // limpiamos el código quitándole comentarios, caracteres especiales y espacios de más
            var resultado = texto.replacingOccurrences(of: comentarios, with: " ", options:.regularExpression)
            resultado = resultado.replacingOccurrences(of: especial, with: " ", options:.regularExpression)
            resultado = resultado.replacingOccurrences(of: espaciosAUno, with: " ", options:.regularExpression)
            print(resultado)
            
            // obtenemos el índice y el caracter en la iteración 
            for (i, char) in resultado.enumerated() {
                // si el caracter es diferente del espacio
                if char != " " || char == "'" {
                    // súmale el caracter actual al lexema
                    lexema = lexema + String(char)
                }
                
                if i+1 < resultado.count {
                    if resultado[ resultado.index(resultado.startIndex, offsetBy: i+1) ] == " " || // hola_
                    simbolos.contains( String( resultado[ resultado.index(resultado.startIndex, offsetBy: i+1) ] ) ) || // 
                    simbolos.contains(lexema) {
                        // si el lexema no está vacía
                        if lexema != "" {
                            // agregamos a la lista de lexemas
                            lexemas.append(lexema)
                            // y vaciamos el lexema
                            lexema = ""
                        }
                    }
                }
            }

            // print(lexemas)
            print("\n-------------------")
            print("Tokens")
            print("-------------------")
            
            // por cada lexema en los lexemas
            for lexema in lexemas {
                // obtenemos el index y el elemento (elemento tiene key y value dentro) que está en los tokens
                test: for (index, element) in tokens.enumerated() {
                    // verifica si el lexema se parece a la llave
                    if lexema.range(of: element.key, options:.regularExpression) != nil {
                        // si se parece, mostramos el token y el lexema
                        print("\(element.value) \(lexema)")

                        escaneado.append(element.value)
                        escaneadoT = escaneadoT + element.value + " "

                        // después rompemos el ciclo
                        break test
                    }

                    // si llegamos al final de la iteración
                    if index == tokens.count - 1 {
                        // mostramos el identificador
                        print("ID \(lexema)")

                        escaneado.append("ID")
                        escaneadoT = escaneadoT + "ID "
                        
                        // verificamos que no haya estado en la tabla de símbolos antes
                        if !tablaDeSimbolos.contains{ $0.1 == lexema } {
                            tablaDeSimbolos.append((numeroDeSimbolo, lexema)) // agregamos nuevo identificador
                            numeroDeSimbolo += 1
                        } else {
                            if let index = tablaDeSimbolos.firstIndex(where: { $0.1 == lexema }) {
                                tablaDeSimbolos.append(tablaDeSimbolos[index])
                            }
                        }
                    }
                }
            }

            // mostramos la tabla de símbolos
            print("\n-------------------")
            print("Tabla de símbolos")
            print("-------------------")
            
            for (key, value) in tablaDeSimbolos {
                print("\(key) \(value)")
            }

            // mostramos lo que se escaneó
            print("\n-------------------")
            print("Tokens escaneados")
            print("-------------------")
            print(escaneado)
            print(escaneadoT)
            print()
        }
        catch {
            // si entramos aquí significa que no pudimos encontrar al archivo
            print("Archivo no encontrado")
        }
    }
} else {
    // algo pasó
    print("Error")
}

//////////////////////////////////
//          Sintáctico          //
//////////////////////////////////

// let instrMain = ["PRINCIPAL", "ILLAVE", "DLLAVE"]

func sintaxear(_ escaneado: [String]) {
    var llaves: Int = 0
    var instruccionTipo: String = ""
    var instruccion: String = ""
    var dentroDeEscaneado = [String]()

    for (index, token) in escaneado.enumerated() {
        if llaves == 0 {
            if token == "CLASE" || token == "TIPO_VAR_ARR" || token == "FUNCION" {
                instruccionTipo = obtenerTipoDeInstruccion(token)
            }

            instruccion = instruccion + token + " "

            if token == "PUNTO_COMA" {
                rev(instruccionTipo, instruccion)
                instruccion = ""
            }
        } else {
            dentroDeEscaneado.append(token)
        }
    
        if token == "ILLAVE" {
            llaves = llaves + 1
        } else if token == "DLLAVE" {
            llaves = llaves - 1

            if llaves == 0 {
                if token == "CLASE" {
                    instruccionTipo = "CLASE"
                }
                instruccion = instruccion + token + " "
            }
        }
    }

    if !dentroDeEscaneado.isEmpty {
        dentroDeEscaneado.removeLast()
    }

    if llaves == 0 {
        rev(instruccionTipo, instruccion)

        print(instruccion)
        if !dentroDeEscaneado.isEmpty {
            sintaxear(dentroDeEscaneado)
        }
    } else {
        print("Error sintáctico")
    }
}

func rev(_ instruccionTipo: String, _ instruccion: String) {
    switch instruccionTipo {
    case "CLASE":
        if instruccion.range(of: instrClase(), options:.regularExpression) != nil {
            print("Ok instrucción de clase")
        } else {
            print("Error sintáctico en la clase")
        }
    case "TIPO_VAR_ARR":
        if instruccion.range(of: declaracionArray(), options:.regularExpression) != nil {
            print("Ok instrucción array")
        } else {
            print("Error sintáctico en array")
        }
    case "FUNCION":
        if instruccion.range(of: instrFunc(), options:.regularExpression) != nil {
            print("Ok instrucción funcion")
        } else {
            print("Error sintáctico en funcion")
        }
    default:
        break
    }
}

func obtenerTipoDeInstruccion(_ token: String) -> String {
    switch token {
    case "CLASE":
        return "CLASE"
    case "TIPO_VAR_ARR":
        return "TIPO_VAR_ARR"
    case "FUNCION":
        return "FUNCION"
    default:
        break
    }
    
    return "ERR"
}

func check(_ escaneado: String) {
    // let programa: String = "\(instrClase())|\(instrFunc())\\s"

    if escaneado.range(of: instrClase(), options:.regularExpression) != nil {
        print("OK instrClase")
    } else {
        print("SYNTAX ERR: instrClase")
        print(" ", instrClase())
    }

    if escaneado.range(of: declaracionArray(), options:.regularExpression) != nil {
        print("OK declaracionArray")
    } else {
        print("SYNTAX ERR: declaracionArray")
        print(" ", declaracionArray())
    }
    
    if escaneado.range(of: instrFunc(), options:.regularExpression) != nil {
        print("OK instrFunc")
    } else {
        print("SYNTAX ERR: instrFunc")
        print(" ", instrFunc())
    }

    if escaneado.range(of: cicloWhile(), options:.regularExpression) != nil {
        print("OK cicloWhile")
    } else {
        print("SYNTAX ERR: cicloWhile")
        print(" ", cicloWhile())
    }

    if escaneado.range(of: manejadorErrores(), options:.regularExpression) != nil {
        print("OK manejadorErrores")
    } else {
        print("SYNTAX ERR: manejadorErrores")
        print(" ", manejadorErrores())
    }

    if escaneado.range(of: declaracionVar(), options:.regularExpression) != nil {
        print("OK declaracionVar")
    } else {
        print("SYNTAX ERR: declaracionVar")
        print(" ", declaracionVar())
    }

    if escaneado.range(of: declaracionDict(), options:.regularExpression) != nil {
        print("OK declaracionDict")
    } else {
        print("SYNTAX ERR: declaracionDict")
        print(" ", declaracionDict())
    }
}


// func instrMain() -> String {
//     let instruccion = "(PRINCIPAL ILLAVE DLLAVE)"
// }

func instrClase() -> String {
    let clasePadre: String = "(\\sID)?"
    let comportamiento: String = "(\\sPUNTUACION ID)?"
    let instruccion = "(AMBITO_ACCESO CLASE ID IPARENT\(clasePadre) DPARENT\(comportamiento) ILLAVE DLLAVE)"

    return instruccion
}

func declaracionArray() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "CADENA"
    let dato: String = "(\(entero)|\(real)|\(cadena))"
    let otrosDatos: String = "COMA \(dato)"
    let datos: String = "(\(dato)\\s?(\(otrosDatos))*)?"
    let instruccion = "(TIPO_VAR_ARR TIPO_VAR ID ASIGNACION ICORCHET\\s\(datos)\\s?DCORCHET PUNTO_COMA)"

    return instruccion
}

func declaracionVar() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "CADENA"
    let dato: String = "(\(entero)|\(real)|\(cadena))"
    let conValor:String = "(ASIGNACION \(dato))?"
    let instruccion: String = "(TIPO_VAR ID \(conValor) PUNTO_COMA)"

    return instruccion
}

func instrFunc() -> String {
    let parametro: String = "TIPO_VAR ID"
    let otrosParametros: String = "\\sCOMA \(parametro)"
    let parametros: String = "(\(parametro)(\(otrosParametros))*)?"
    let funcionTipo: String = "PUNTUACION TIPO_VAR"
    let instruccion: String = "(FUNCION ID IPARENT\\s\(parametros)\\s?DPARENT \(funcionTipo) ILLAVE DLLAVE)"
    
    return instruccion
}

func cicloWhile() -> String {
    let condicion: String = "ID RELACIONAL ID"
    let otrasCondiciones: String = "LOGICO \(condicion)"
    let condiciones: String = "\(condicion) (\(otrasCondiciones))*"
    let instruccion: String = "(MIENTRAS IPARENT \(condiciones)\\s?DPARENT ILLAVE DLLAVE)"

    return instruccion
}

//*
func declaracionDict() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "CADENA"
    let dato: String = "\(cadena) PUNTUACION (\(entero)|\(real)|\(cadena))"
    let otrosDatos: String = "COMA \(dato)"
    let datos: String = "(\(dato)\\s?(\(otrosDatos))*)?"
    let instruccion: String = "(TIPO_VAR_DICT TIPO_VAR ID ASIGNACION ILLAVE \(datos) DLLAVE PUNTO_COMA)"
    
    return instruccion
}

func manejadorErrores() -> String {
    return "(EXCEPCION_TRY ILLAVE DLLAVE EXCEPCION_CATCH IPARENT ERROR_TIPO DPARENT ILLAVE DLLAVE)"
}

//*
func lecturaDatos() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
    let instruccion: String = "(ENTRADA IPARENT \(cadena) DPARENT PUNTO_COMA)"
    
    return instruccion
}

//*
func instrSwitch() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
    let dato: String = "\(cadena) PUNTUACION (\(entero)|\(real)|\(cadena))"
    let bloqueCase: String = "SWITCH_AUX \(dato) PUNTUACION ILLAVE ROMPER PUNTO_COMA DLLAVE"
    let bloqueDef: String = "SWITCH_DEF PUNTUACION ILLAVE ROMPER PUNTO_COMA DLLAVE"
    let instruccion: String = "(PREGUNTA IPARENT condicion DPARENT ILLAVE \(bloqueCase) \(bloqueDef) DLLAVE)"

    return instruccion
}

//*
func cicloFor() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
    let rango: String = "(\(entero)|\(real)|\(cadena))|\(cadena)"
    let instruccion: String = "(PARA IPARENT TIPO_VAR ID EN \(rango) DPERENT ILLAVE)"

    return instruccion
}

func condicionIf() -> String {
    let condicion: String = "ID RELACIONAL ID"
    let otrasCondiciones: String = "LOGICO \(condicion)"
    let condiciones: String = "\(condicion) (\(otrasCondiciones))*"
    let instruccion: String = "(PREGUNTA IPARENT \(condiciones) DPARENT ILLAVE DLLAVE)"

    return instruccion
}

//*
func declaracionLista() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
    let dato: String = "(\(entero)|\(real)|\(cadena))"
    let otrosDatos: String = "COMA \(dato)"
    let conValor: String = "(\(dato)\\s?(\(otrosDatos))*)?"
    let instruccion: String = "(TIPO_VAR LISTA ID \(conValor) PUNTO_COMA)"

    return instruccion
}

//*
func salidaDatos() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
    let dato: String = "(\(entero)|\(real)|\(cadena))"
    let instruccion: String = "(SALIDA IPARENT \(dato) DPARENT)"

    return instruccion
}

//*
func usoLibreria() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
    let instruccion: String = "IMPORTAR ID DE \(cadena)"

    return instruccion
}

//*
func ConexionMySql() -> String {
    let entero: String = "ENTERO"
    let real: String = "\(entero) PUNTO \(entero)"
    let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
    let conValor: String = "(ASIGNACION \(cadena))?"
    let instruccion: String = "CONEXION ID \(conValor) PUNTO_COMA"

    return instruccion
}

// func throwError() -> String {
//     let cadena: String = "COMILLA_SIMPLE (ID|ENTERO|\(real)) COMILLA_SIMPLE"
//     let instruccion: String = "llll"

//     return instruccion
// }

check(escaneadoT)
// sintaxear(escaneado)