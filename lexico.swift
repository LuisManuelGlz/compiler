import Foundation

var simbolos = ["{", "}", "(", ")", "[", "]", ".", ":", ",", "=", "'"]

var comentarios = "(/\\*([^*]|[\r\n]|(\\*+([^*/]|[\r\n])))*\\*+/)|(//.*)"
var especial = "\n"
var espaciosAUno = "\\s+"

var index = 0
var lexema = ""
var lexemas = [String]()
var numeroDeSimbolo = 0
var tablaDeSimbolos = [(Int, String)]()

// queda pendiente reducir todo esto
var reglas = [
    // "^[a-zA-Z_]\\w*$": "IDENTIFICADOR",
    "^\\d+$": "NUMERO",
    "^=$": "ASIGNACION",
    "^\\($": "IPARENT",
    "^\\)$": "DPARENT",
    "^\\[$": "ICORCHET",
    "^\\]$": "DCORCHET",
    "^\\{$": "ILLAVE",
    "^begin$": "ILLAVE",
    "^\\}$": "DLLAVE",
    "^end$": "DLLAVE",
    "^if$": "CONDICION",
    "^switch$": "CONDICION",
    "^else$": "CONDICION_AUX",
    "^elif$": "CONDICION_AUX",
    "^while$": "BUCLE",
    "^for$": "BUCLE",
    "^foreach$": "BUCLE",
    "^do$": "BUCLE",
    "^break$": "BUCLE",
    "^continue$": "BUCLE",
    "^public$": "AMBITO",
    "^private$": "AMBITO",
    "^protected$": "AMBITO",
    "^global$": "AMBITO",
    "^\\.$": "PUNTUACION",
    "^,$": "PUNTUACION",
    "^;$": "PUNTUACION",
    "^:$": "PUNTUACION",
    // "^\"$": "PUNTUACION",
    "^'$": "PUNTUACION",
    "^`$": "PUNTUACION",
    "^\\+$": "ARITMETICO",
    "^\\-$": "ARITMERICO",
    "^/$": "ARITMERICO",
    "^%$": "ARITMERICO",
    "^\\+\\+$": "ARITMERICO",
    "^\\+=$": "ARITMERICO",
    "^\\-\\-$": "ARITMERICO",
    "^\\-=$": "ARITMERICO",
    "^\\*\\*$": "ARITMERICO",
    "^\\*=$": "ARITMERICO",
    "^/=$": "ARITMERICO",
    "^==$": "RELACIONAL",
    "^!=$": "RELACIONAL",
    "^<$": "RELACIONAL",
    "^>$": "RELACIONAL",
    "^<=$": "RELACIONAL",
    "^>=$": "RELACIONAL",
    "^not$": "LOGICO",
    "^!$": "LOGICO",
    "^and$": "LOGICO",
    "^\\&\\&$": "LOGICO",
    "^or$": "LOGICO",
    "^\\|\\|$": "LOGICO",
    "^xor$": "LOGICO",
    "^case$": "SWITCH_AUX",
    "^default$": "SWITCH_AUX",
    "^array$": "TIPO_VAR",
    "^int$": "TIPO_VAR",
    "^dict$": "TIPO_VAR",
    "^bool$": "TIPO_VAR",
    "^char$": "TIPO_VAR",
    "^enum$": "TIPO_VAR",
    "^volatile$": "TIPO_VAR",
    "^void$": "TIPO_VAR",
    "^var$": "TIPO_VAR",
    "^string$": "TIPO_VAR",
    "^decimal$": "TIPO_VAR",
    "^float$": "TIPO_VAR",
    "^double$": "TIPO_VAR",
    "^const$": "TIPO_VAR",
    "^byte$": "TIPO_VAR",
    "^sbyte$": "TIPO_VAR",
    "^uint$": "TIPO_VAR",
    "^func$": "FUNCION",
    "^function$": "FUNCION",
    "^def$": "FUNCION",
    "^=>$": "FUNCION_FLECHA",
    "^lambda$": "FUNCION",
    "^then$": "EXCEPTION",
    "^try$": "EXCEPTION",
    "^catch$": "EXCEPTION",
    "^finally$": "EXCEPTION_AUX",
    "^small$": "NUMERO_LONGITUD",
    "^long$": "NUMERO_LONGITUD",
    "^ulong$": "NUMERO_LONGITUD",
    "^ushort$": "NUMERO_LONGITUD",
    "^true$": "BOOLEANO",
    "^false$": "BOOLEANO",
    "^static$": "STATIC_VAR",
    "^yield$": "ITERADOR_AUX",
    "^List$": "ESTRUCTURA",
    "^LinkedList$": "ESTRUCTURA",
    "^Stack$": "ESTRUCTURA",
    "^Queue$": "ESTRUCTURA",
    "^Stacklloc$": "ESTRUCTURA",
    "^ValueError$": "ERROR_TIPO",
    "^TypeError$": "ERROR_TIPO",
    "^NameError$": "ERROR_TIPO",
    "^ZeroDivisionError$": "ERROR_TIPO",
    "^OsError$": "ERROR_TIPO",
    "^RuntimeError$": "ERROR_TIPO",
    "^Exception$": "ERROR_TIPO",
    "^max$": "OPERACION_NUMERICA",
    "^min$": "OPERACION_NUMERICA",
    "^avg$": "OPERACION_NUMERICA",
    "^sum$": "OPERACION_NUMERICA",
    "^roof$": "OPERACION_NUMERICA",
    "^ceil$": "OPERACION_NUMERICA",
    "^floor$": "OPERACION_NUMERICA",
    "^Thread$": "HILO",
    "^Date$": "FECHA",
    "^Datetime$": "FECHA",
    "^Year$": "FECHA",
    "^Mounth$": "FECHA",
    "^Day$": "FECHA",
    "^Hour$": "FECHA",
    "^Minutes$": "FECHA",
    "^Seconds$": "FECHA",
    "^equals$": "CHEQUEO",
    "^isEmpty$": "CHEQUEO",
    "^instanceOf$": "CHEQUEO",
    "^lengthOf$": "CHEQUEO",
    "^indexOf$": "CHEQUEO",
    "^startIndexOf$": "CHEQUEO",
    "^endIndexOf$": "CHEQUEO",
    "^count$": "CHEQUEO",
    "^isStr$": "CHEQUEO",
    "^isInt$": "CHEQUEO",
    "^isSet$": "CHEQUEO",
    "^typeOf$": "CHEQUEO",
    "^print$": "DESPLEGAR",
    "^write$": "DESPLEGAR",
    "^writeLine$": "DESPLEGAR",
    "^abstract$": "TIPO_DE_CLASE",
    "^input$": "ENTRADA",
    "^prompt$": "ENTRADA",
    "^read$": "ENTRADA",
    "^readLine$": "ENTRADA",
    "^\\$$": "VAR_EN_CADENA",
    "^args$": "ARGUMENTO",
    "^kwargs$": "ARGUMENTO",
    "^override$": "COMPLETENTO_CLASE",
    "^virtual$": "COMPLETENTO_CLASE",
    "^subscribe$": "PALABRA_CLAVE",
    "^unsubscribe$": "PALABRA_CLAVE",
    "^path$": "PALABRA_CLAVE",
    "^trim$": "PALABRA_CLAVE",
    "^bind$": "PALABRA_CLAVE",
    "^get$": "PALABRA_CLAVE",
    "^set$": "PALABRA_CLAVE",
    "^destroy$": "PALABRA_CLAVE",
    "^destructor$": "PALABRA_CLAVE",
    "^init$": "PALABRA_CLAVE",
    "^constructor$": "PALABRA_CLAVE",
    "^module$": "PALABRA_CLAVE",
    "^export$": "PALABRA_CLAVE",
    "^from$": "PALABRA_CLAVE",
    "^import$": "PALABRA_CLAVE",
    "^package$": "PALABRA_CLAVE",
    "^native$": "PALABRA_CLAVE",
    "^synchronized$": "PALABRA_CLAVE",
    "^using$": "PALABRA_CLAVE",
    "^as$": "PALABRA_CLAVE",
    "^goto$": "PALABRA_CLAVE",
    "^join$": "PALABRA_CLAVE",
    "^value$": "PALABRA_CLAVE",
    "^strict$": "PALABRA_CLAVE",
    "^async$": "PALABRA_CLAVE",
    "^await$": "PALABRA_CLAVE",
    "^readOnly$": "PALABRA_CLAVE",
    "^event$": "PALABRA_CLAVE",
    "^delegate$": "PALABRA_CLAVE",
    "^super$": "PALABRA_CLAVE",
    "^in$": "PALABRA_CLAVE",
    "^fetch$": "PALABRA_CLAVE",
    "^Schema$": "PALABRA_CLAVE",
    "^Number$": "PALABRA_CLAVE",
    "^Integer$": "PALABRA_CLAVE",
    "^String$": "PALABRA_CLAVE",
    "^Array$": "PALABRA_CLAVE",
    "^Dictionary$": "PALABRA_CLAVE",
    "^pass$": "PALABRA_CLAVE",
    "^class$": "PALABRA_CLAVE",
    "^this$": "PALABRA_CLAVE",
    "^return$": "PALABRA_CLAVE",
    "^Convert$": "PALABRA_CLAVE",
    "^new$": "PALABRA_CLAVE",
    "^extends$": "PALABRA_CLAVE",
    "^interface$": "PALABRA_CLAVE",
    "^throw$": "PALABRA_CLAVE",
    "^throws$": "PALABRA_CLAVE",
    "^null$": "PALABRA_CLAVE",
    "^none$": "PALABRA_CLAVE",
    "^undefined$": "PALABRA_CLAVE",
    "^final$": "PALABRA_CLAVE",
    "^assert$": "PALABRA_CLAVE",
    "^obj$": "PALABRA_CLAVE",
    "^die$": "PALABRA_CLAVE",
    "^delete$": "PALABRA_CLAVE",
    "^exec$": "PALABRA_CLAVE",
    "^debugger$": "PALABRA_CLAVE",
    "^exit$": "PALABRA_CLAVE",
    "^IO$": "PALABRA_CLAVE",
    "^File$": "PALABRA_CLAVE",
    "^Mail$": "PALABRA_CLAVE",
    "^Math$": "PALABRA_CLAVE",
    "^open$": "PALABRA_CLAVE",
    "^close$": "PALABRA_CLAVE",
    "^Show$": "PALABRA_CLAVE",
    "^Hidde$": "PALABRA_CLAVE",
    "^range$": "PALABRA_CLAVE",
    "^process$": "PALABRA_CLAVE",
    "^send$": "PALABRA_CLAVE",
    "^Time$": "PALABRA_CLAVE",
    "^with$": "PALABRA_CLAVE",
    "^crypto$": "PALABRA_CLAVE",
    "^scrennX$": "PALABRA_CLAVE",
    "^screenY$": "PALABRA_CLAVE",
    "^transient$": "PALABRA_CLAVE",
    "^out$": "PALABRA_CLAVE",
    "^base$": "PALABRA_CLAVE",
    "^implicit$": "PALABRA_CLAVE",
    "^lock$": "PALABRA_CLAVE",
    "^ref$": "PALABRA_CLAVE",
    "^System$": "PALABRA_CLAVE",
    "^internal$": "PALABRA_CLAVE",
    "^clone$": "PALABRA_CLAVE",
    "^copy$": "PALABRA_CLAVE",
    "^eval$": "PALABRA_CLAVE",
    "^include_once$": "PALABRA_CLAVE",
    "^Http$": "PALABRA_CLAVE",
    "^to$": "PALABRA_CLAVE",
    "^MySqlConnection$": "PALABRA_CLAVE",
    "^MySqlCommand$": "PALABRA_CLAVE",
    "^PgSqlConnection$": "PALABRA_CLAVE",
    "^PgSqlCommand$": "PALABRA_CLAVE",
    "^MsSqlConnection$": "PALABRA_CLAVE",
    "^MsSqlCommand$": "PALABRA_CLAVE",
    "^pow$": "MATE_FUNC",
    "^sqrt$": "MATE_FUNC",
    "^exp$": "MATE_FUNC",
    "^log$": "MATE_FUNC",
    "^sin$": "MATE_FUNC",
    "^cos$": "MATE_FUNC",
    "^tan$": "MATE_FUNC",
    "^asin$": "MATE_FUNC",
    "^acos$": "MATE_FUNC",
    "^atang$": "MATE_FUNC",
    "^log10$": "MATE_FUNC"
]

print("Introduce el archivo:")

// leemos lo que introduzca el usuario
if let fuente = readLine() {
    if let directorio = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

        // definimos la URL
        let ruta = directorio.appendingPathComponent("ITT/7/LyA1/compiler/" + fuente)

        // leyendo archivo
        do {
            // tratamos de leer el archivo con la URL
            var texto = try String(contentsOf: ruta, encoding: .utf8)
            
            // este espacio es necesario, de lo contrario no se leera la última lexema
            texto = texto + " "

            // limpiamos el código quitándole comentarios, caracteres especiales y espacios de más
            var resultado = texto.replacingOccurrences(of: comentarios, with: " ", options:.regularExpression)
            resultado = resultado.replacingOccurrences(of: especial, with: " ", options:.regularExpression)
            resultado = resultado.replacingOccurrences(of: espaciosAUno, with: " ", options:.regularExpression)
            print(resultado)
            
            // obtenemos el índice y el caracter en la iteración 
            for (i, char) in resultado.enumerated() {
                // si el caracter es diferente del espacio
                if char != " " {
                    // súmale el caracter actual a la lexema
                    lexema = lexema + String(char)
                }
                
                if i+1 < resultado.count {
                    if resultado[resultado.index(resultado.startIndex, offsetBy: i+1)] == " " || 
                    simbolos.contains(String(resultado[resultado.index(resultado.startIndex, offsetBy: i+1)])) ||
                    simbolos.contains(lexema) {
                        // si la lexema no está vacía
                        if lexema != "" {
                            // agregamos a la lista de lexemas
                            lexemas.append(lexema)
                            // y vaciamos la lexema
                            lexema = ""
                        }
                    }
                }
            }

            // print(lexemas)
            print("\n-------------------")
            print("Tokens")
            print("-------------------")
            
            // por cada lexema en las lexemas
            for lexema in lexemas {
                // obtenemos el index y el elemento (elemento tiene key y value dentro) que está en las reglas
                test: for (index, element) in reglas.enumerated() {
                    // verifica si la lexema se parece a la llave
                    if lexema.range(of: element.key, options:.regularExpression) != nil {
                        // si se parece, mostramos el token y el lexema
                        print("\(element.value) \(lexema)")

                        // después rompemos el ciclo
                        break test
                    }

                    // si llegamos al final de la iteración
                    if index == reglas.count - 1 {
                        // mostramos el identificador
                        print("IDENTIFICADOR \(lexema)")
                        // ts = [(0, "hola"), (1, "abc")]
                        // verificamos que no haya estado en la tabla de símbolos antes
                        if !tablaDeSimbolos.contains{ $0.1 == lexema } {
                            tablaDeSimbolos.append((numeroDeSimbolo, lexema)) // agregamos nuevo identificador
                            numeroDeSimbolo += 1
                        } else {
                            if let index = tablaDeSimbolos.firstIndex(where: { $0.1 == lexema }) {
                                tablaDeSimbolos.append(tablaDeSimbolos[index])
                            }
                        }
                    }
                }
            }

            // mostramos la tabla de símbolos
            print("\n-------------------")
            print("Tabla de símbolos")
            print("-------------------")
            
            // mostramos la tabla de símbolos
            for (key, value) in tablaDeSimbolos {
                print("\(key) \(value)")
            }
        }
        catch {
            // si entramos aquí significa que no pudimos encontrar al archivo
            print("Archivo no encontrado")
        }
    }
} else {
    // algo pasó
    print("Error")
}

